This is the repository of the GHap R package version 2.0.0.
It is mantained by:

	1. Yuri Tani Utsunomiya
	2. Marco Milanesi

Basic commands for git usage via shell are listed for your convenience:

# Check the status of your copy of the repo

	git status

# Add a file to the track

	git add FILENAME

# Commit the changes to the repo (locally)

	git commit -m "Instructions on the files you are committing"

# Push the changes to the online repository

	git push -u origin master
	
# To use the package
Download the .tar.gz file and build the package. 

